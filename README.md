# Simple password gen
Just a password and mail generator with writing to a file
## Installation
Install random
```bash
pip install random

pip install art
```
## Usage
```python
#Your email without domain
mail = list("YOUR EMAIL WITHOUT DOMAIN")

#Your email domain
mail = ''.join(mail) + "YOUR DOMAIN"
```
## License
[GPL-3.0](https://choosealicense.com/licenses/gpl-3.0/)